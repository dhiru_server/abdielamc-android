package com.example.us.abdiel.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



/**
 * Created by Braintech on 9/13/2016.
 */
public class AlertDialogHelper {


    public static void showAlert(Context context, String msg) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(msg);
            builder.setCancelable(true);

            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert = builder.create();
            alert.show();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    public static void showAlertFinish(final Context context, String msg) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(msg);
            builder.setCancelable(true);

            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ((Activity) context).finish();
                            dialog.cancel();
                        }
                    });


            AlertDialog alert = builder.create();
            alert.show();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    public static void showAlertDesc(Context context, String msg) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(msg);
            builder.setCancelable(true);

            builder.setPositiveButton(
                    "Close",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert = builder.create();
            alert.show();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    public static void showCompleteAlert(final Context context, String msg) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(msg);
            builder.setCancelable(true);

            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            ((Activity) context).finish();
                        }
                    });


            AlertDialog alert = builder.create();
            alert.show();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

}
