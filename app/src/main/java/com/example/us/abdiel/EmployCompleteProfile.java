package com.example.us.abdiel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployCompleteProfile extends AppCompatActivity {


    @BindView(R.id.etEmpFullName)
    EditText etEmpFullName;

    @BindView(R.id.etEmpPhone)
    EditText etEmpPhoneNumber;

    @BindView(R.id.etEmpID)
    EditText etEmpID;
    @BindView(R.id.etEmpAddress)
    EditText etEmpAddress;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employ_complete_profile);


        ButterKnife.bind(this);



    }
}
