package com.example.us.abdiel;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.us.abdiel.Helper.Progress;

public class EmployeeActivity extends AppCompatActivity {


    private FirebaseAuth mAuth;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.btnLoginRestraionToggle)
    Button btnLoginRestraionToggle;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.txtSingInLable)
    TextView txtSingInLable;

    Boolean isLoginType ;

    String TAG = "EmployLoginActivity";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);


        ButterKnife.bind(this);

        isLoginType = true;

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null)
        {
            Intent intent = new Intent(EmployeeActivity.this,EmployeeActivity.class);
            startActivity(intent);
            finish();

        }else
        {


        }


        btnLoginRestraionToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isLoginType = ! isLoginType;
                updateUI();

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()) {
                    if (isLoginType) {

                        doLogin();
                    } else {

                        doRegistration();
                    }
                }
            }



        });

        /// updateUI(currentUser);

    }

    public void doLogin()
    {
        Progress.start(this);
        mAuth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                       Progress.stop();

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();



                            boolean emailVerified = user.isEmailVerified();

                            if (emailVerified)
                            {

                                String strEmail = user.getEmail();
                                String strName = user.getDisplayName();
                                Uri urlImage = user.getPhotoUrl();


                                Log.w(TAG, "signInWithEmail:failure", task.getException());


                                Toast.makeText(EmployeeActivity.this, "Login Succcessfull.",
                                        Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(EmployeeActivity.this,EmployeeActivity.class);
                                intent.putExtra("email",strEmail);
                                intent.putExtra("name",strName);
                                // intent.putExtra("imageUri",urlImage);

                                startActivity(intent);
                                finish();

                            }
                            else
                            {
                                Toast.makeText(EmployeeActivity.this, "Please verify email first",
                                        Toast.LENGTH_SHORT).show();

                            }



                            //updateUI(user);
                        } else {


                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmployeeActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            // updateUI(null);
                        }

                        // ...
                    }
                });

    }


    public void doRegistration()
    {

        Progress.start(this);
        mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {


                        Progress.stop();

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // updateUI(user);
                            user.sendEmailVerification();



                            Toast.makeText(EmployeeActivity.this, "Registration Succcessfull. A verification link has been sent to your email",
                                    Toast.LENGTH_SHORT).show();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(EmployeeActivity.this, "Registration failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });

    }

    public boolean validation()
    {

        if (etEmail.getText().toString().length() == 0)
        {
            Toast.makeText(this,"Enter your email"  ,Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!isValidEmail(etEmail.getText().toString()))
        {
            Toast.makeText(this,"Please enter valid email address"  ,Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etPassword.getText().toString().length() == 0)
        {
            Toast.makeText(this,"Enter your Password"  ,Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void updateUI()
    {

        if (isLoginType)
        {
            btnLogin.setText("Login");
            btnLoginRestraionToggle.setText("Register Now");
            txtSingInLable.setText("Sing In");
        }
        else
        {
            btnLogin.setText("Register");

            btnLoginRestraionToggle.setText("Login Now");

            txtSingInLable.setText("Sing Up");

        }


    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}
