package com.example.us.abdiel.session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mac on 08/04/18.
 */

public class GlobleManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "globle_manager_pref";

    // All Shared Preferences Keys
    private static final String LOGIN_STATUS = "logging_status";


    public GlobleManager(Context _context) {
        this.context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void saveEmail(String email) {
        editor.putString(LOGIN_STATUS, email);
        // commit changes
        editor.commit();
    }

}
