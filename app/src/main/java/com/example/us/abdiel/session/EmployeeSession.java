package com.example.us.abdiel.session;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mac on 08/04/18.
 */

public class EmployeeSession {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "employee_pref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "is_user_logged_in";

    public static final String KEY_USER_ID = "user_id";

    public static final String KEY_USER_NAME = "username";

    public static final String KEY_USER_TYPE = "userType";

    public static String KEY_SELECTED_DEVICE = "isSelectedDevice";

    public static final String KEY_TEMP_ID = "tempId";

    public static final String KEY_EMAIL = "email";

    public static final String KEY_LAT = "lat";

    public static final String KEY_LNG = "lng";


    public static final String KEY_FIRSTNAME = "fname";
    public static final String KEY_LASTNAME = "lname";


    public static final String KEY_CARDNUMBER = "card_number";
    public static final String KEY_CARDNAME = "card_name";
    public static final String KEY_EXPMONTH = "exp_month";
    public static final String KEY_EXPYEAR = "exp_year";

    public static final String KEY_DISPLAY_STATUS = "display_status";


    public EmployeeSession(Context _context) {
        this.context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createUserSession(String user_id, int userType, String userName, boolean isSelectedDevice) {

        editor.putString(KEY_USER_ID, user_id);
        editor.putBoolean(IS_LOGIN, true);
        editor.putInt(KEY_USER_TYPE, userType);
        editor.putString(KEY_USER_NAME, userName);
        editor.putBoolean(KEY_SELECTED_DEVICE, isSelectedDevice);

        // commit changes
        editor.commit();
    }


    /**
     * Create login session
     */
    public void createCardSession(String cardNumber, String cardName, String expMonth, String expYear) {

        editor.putString(KEY_CARDNUMBER, cardNumber);
        editor.putString(KEY_CARDNAME, cardName);
        editor.putString(KEY_EXPMONTH, expMonth);
        editor.putString(KEY_EXPYEAR, expYear);

        // commit changes
        editor.commit();
    }

    public void createDashboardUserSession(String firstName, String lastName) {

        editor.putString(KEY_FIRSTNAME, firstName);
        editor.putString(KEY_LASTNAME, lastName);

        // commit changes
        editor.commit();
    }


    public void createLatLng(String lat, String lng) {

        editor.putString(KEY_LAT, lat);
        editor.putString(KEY_LNG, lng);

        // commit changes
        editor.commit();
    }


    public void saveEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        // commit changes
        editor.commit();
    }

    public void clearUserSession() { // Clearing all data from Shared
        editor.clear();
        editor.commit();
    }

    public void saveTempId(String tempId) {
        editor.putString(KEY_TEMP_ID, tempId);
        editor.commit();
    }

    public void saveDisplayStatus(String status) {
        editor.putString(KEY_DISPLAY_STATUS, status);
        editor.commit();
    }


    public String getDisplayStatus() {
        return pref.getString(KEY_DISPLAY_STATUS, "");
    }

    public String getUserId() {
        return pref.getString(KEY_USER_ID, null);
    }


    public String getTempId() {
        return pref.getString(KEY_TEMP_ID, null);
    }

    // Get Login State
    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean isSelectedDevice() {
        return pref.getBoolean(KEY_SELECTED_DEVICE, false);
    }

    public String getName() {
        return pref.getString(KEY_USER_NAME, null);

    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, null);
    }

    public String getFName() {
        return pref.getString(KEY_FIRSTNAME, null);
    }

    public String getLName() {
        return pref.getString(KEY_LASTNAME, null);
    }


    public int getUserType() {
        return pref.getInt(KEY_USER_TYPE, 0);
    }


    public String getLat() {
        return pref.getString(KEY_LAT, null);
    }

    public String getLng() {
        return pref.getString(KEY_LNG, null);
    }


    public String getCardnumber() {
        return pref.getString(KEY_CARDNUMBER, null);
    }

    public String getCardname() {
        return pref.getString(KEY_CARDNAME, null);
    }

    public String getExpmonth() {
        return pref.getString(KEY_EXPMONTH, null);
    }

    public String getExpyear() {
        return pref.getString(KEY_EXPYEAR, null);
    }

}
